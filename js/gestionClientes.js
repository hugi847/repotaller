var getClentes = function() {
  get('https://services.odata.org/V4/Northwind/Northwind.svc/Customers', {
    $filter: "Country eq 'Germany'"
  }).done(function(response) {
    var content = '';
    $.each(response.value, function(index, customer) {
      customer.Country =
        customer.Country === 'UK' ? 'United-Kingdom' : customer.Country;
      content +=
        '<tr><td>' +
        customer.ContactName +
        '</td><td>' +
        customer.City +
        '</td><td>' +
        '<img class="card-img-top" src="https://www.countries-ofthe-world.com/flags-normal/flag-of-' +
        customer.Country +
        '.png" alt="' +
        customer.Country +
        '"></td></tr>';
    });

    $('#clientes').html(content);
  });
};

$('#btnObtenerClientes').click(getClentes);
