var getProductos = function() {
  get(
    'https://services.odata.org/V4/Northwind/Northwind.svc/Products',
    {}
  ).done(function(response) {
    var content = '';
    $.each(response.value, function(index, product) {
      content +=
        '<tr><td>' +
        product.ProductName +
        '</td><td>' +
        product.UnitPrice +
        '</td><td>' +
        product.UnitsInStock +
        '</td></tr>';
    });

    $('#productos').html(content);
  });
};

$('#btnObtenerProductos').click(getProductos);
